package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardsPage {

    protected WebDriver driver;

    public DashboardsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public DashboardsPage assertProjectURLDash() {
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:4444/");
        return this;
    }

}
