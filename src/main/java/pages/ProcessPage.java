package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessPage extends HomePage{

    public ProcessPage (WebDriver driver){
        super(driver);
    }

    @FindBy(linkText =  "Add new process")
    private WebElement processCreate;

    public CreateProcessPage goToCreateProcess() {
        processCreate.click();
        return new CreateProcessPage(driver);

    }

    public ProcessPage assertProjectURL() {
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:4444/Projects");
    return this;}




}
