package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {

    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".profile_info_>h2")
    private WebElement welcomeElm;


    @FindBy(css = "a.user-profile")
    private WebElement userProfile;

    @FindBy(css = "a[href*=Logout]")
    private WebElement logoutLnk;


    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(css = "a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement charakteristicMenu;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;

    @FindBy(linkText = "a[href$=Processes]")
    private WebElement processes1Menu;




    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));
        return parent.getAttribute("class").contains("active");
    }

    public ProcessPage goToProcesses() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));

        processesMenu.click();
        return new ProcessPage(driver);
    }


         public DashboardsPage goToDashboard() {
            if (!isParentExpanded(homeNav))
                homeNav.click();

            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));


             dashboardMenu.click();
            return new DashboardsPage(driver);
        }

    public CharacteristicPage goToCharacteristic() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(charakteristicMenu));


        charakteristicMenu.click();
        return new CharacteristicPage(driver);
    }

    public LoginPage logout() {
        userProfile.click();
        logoutLnk.click();

        return new LoginPage(driver);

    }

    public HomePage assertWelcomeElementIsShow() {
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"));
        return this;
    }


}
