package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CharacteristicPage extends HomePage{

    public CharacteristicPage(WebDriver driver) {
        super(driver);
    }

    public CharacteristicPage assertProjectURLCharacteristic() {
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:4444/Characteristics");
        return this;
    }


}

