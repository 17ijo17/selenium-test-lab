package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CreateAccountPage {

    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPassword;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;


    public  CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }


    public  CreateAccountPage typePassword(String Password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(Password);
        return this;
    }

    public  CreateAccountPage confirmPasswod(String confPass) {
        confirmPassword.clear();
        confirmPassword.sendKeys(confPass);
        return this;
    }


}
