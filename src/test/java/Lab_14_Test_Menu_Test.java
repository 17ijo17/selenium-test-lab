import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Test_Menu_Test extends SeleniumBase {

@Test
    public void menuTest(){
    new LoginPage(driver)
            .typeEmail("test@test.com")
            .typePassword("Test1!")
            .submitLogin()
            .goToProcesses()
            .assertProjectURL();
    }

    @Test
    public void goToCharacteristic() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToCharacteristic()
                .assertProjectURLCharacteristic();
    }

    @Test
    public void goToDashboard() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToDashboard()
                .assertProjectURLDash();

    }


}
