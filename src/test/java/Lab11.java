import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab11 extends SeleniumBase{



    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }


    @Test(dataProvider ="getWrongEmails")
    public void logoutTest(String zmienna) {
      /* wylaczone na potrzeby 12 zad
        System.setProperty("webdriver.chrome.driver", "c:/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize(); */

        driver.get("http://localhost:4444/");

         LoginPage loginPage = new LoginPage(driver);

       /* CreateAccountPage createAccountPage =*/ loginPage.createNewUser() /* ; */
        /* CreateAccountPage createAccountPage =*/.typeEmail(zmienna) /* ; */
        /* CreateAccountPage createAccountPage =*/.typePassword("1234567") /* ; */
       /* CreateAccountPage createAccountPage =*/.confirmPasswod("1234567");

        loginPage.assertEmailerrorIsShown("The Email field is not a valid e-mail address.");

    driver.quit();
    }





}
