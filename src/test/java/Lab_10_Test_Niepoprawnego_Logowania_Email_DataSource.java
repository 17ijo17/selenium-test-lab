import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource extends SeleniumBase{
    private WebDriver driver;

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void IncorrectEmailTest(String wrongEmail) {
/* wylaczone na potrzeby 12 zad
        System.setProperty("webdriver.chrome.driver", "c:/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize(); */

        driver.get("http://localhost:4444/");

        /*LoginPage loginPage = */new LoginPage(driver) /*; */
        /* loginPage*/.typeEmail(wrongEmail) /*; */
        /* loginPage*/ .submitLoginWithFailure() /*; */

        /* loginPage*/.assertEmailerrorIsShown("The Email field is not a valid e-mail address.");


        driver.quit();

    }


}



