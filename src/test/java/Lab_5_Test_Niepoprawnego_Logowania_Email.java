import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_5_Test_Niepoprawnego_Logowania_Email {


    @Test

    public void incorrectLoginTestWrongemail() {


        System.setProperty("webdriver.chrome.driver", "c:/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("http://localhost:4444/");

        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("test1@test.com");


        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("Test1!");

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();


        // WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
        //Assert.assertEquals(emailError.getText(), "The email field is not a valid e-amil address");


        List<WebElement> validationErrors = driver.findElements(By.cssSelector("dy > div > div > div > section > form > div.text-danger.validation-summary-errors > ul > li"));
              boolean doesErrorExists = false;
          for (int i = 0; i < validationErrors.size(); i++) {
              if (validationErrors.get(i).getText().equals("The email is not a valid e-mail adress.")) {
               doesErrorExists = true;
             break;
    }
}
      Assert.assertTrue(doesErrorExists);





        driver.quit();
        }
        }
