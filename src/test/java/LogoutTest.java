import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class LogoutTest {

    private WebDriver driver;

    @Test
    public void logoutTest () {
        System.setProperty("webdriver.chrome.driver", "c:/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("http://localhost:4444/");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("Test@test.com");
        loginPage.typePassword("Test1!");
        HomePage homePage = loginPage.submitLogin();
        loginPage=homePage.logout();

        loginPage.assertUserSuccessfullyLogOut();
        driver.quit();


    }

}
